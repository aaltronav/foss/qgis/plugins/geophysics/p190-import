# UKOOA P1/90 file import

Import UKOOA P1/90 files as a QGIS layer or layer group.

![](media/gui.png "P1/90 import dialog")

## Features

* Filter records by type (S, V, E, T, …)
* Partial import based on shotpoint range
* Import by geodetic or grid coordinates
* Import R (receiver) records
* Option to import shots as individual layers (useful when bringing in receivers)


## Usage

The plugin adds a menu action under Plugins → Seismic → Import P190

### Basic usage

* Select a P1/90 file
* Select the record types you would like to import
* Press Ok

### Importing `R` records

As `R` records do not have geographic coordinates, the appropriate grid CRS must be selected in order to import those.

Be aware that importing `R` records can take a lot of time and memory.

![](media/3d-receivers.png "3D import with receivers")

### Partial import

A shotpoint range may be selected by checking the *‘Partial import’* option. Only those records falling within the shotpoint range will be imported.

### Layer options

Each shotpoint may be saved as an individual layer, and those layers may be grouped together. This is handy when importing receiver records and in combination with the *‘Mutually exclusive group’* QGIS option.

![](media/exclusive-layers.png "Mutually exclusive layers control")
