#!/usr/bin/python3

import re

def from_file (path, only_records=None, shot_range=None, progress_callback=None, callback_every=500):
	with open(path) as fd:
		line_count = 0
		records = []
		line_name = None
		point_number = None
		doy = None
		time = None

		line = fd.readline()

		if not line[0] == "H":
			# This is not a P1/90 file
			return

		while line:
			if line[:3] == "EOF":
				break

			record = parse_line(line)
			line = fd.readline()

			if "line_name" in record:
				line_name = record["line_name"]
			elif line_name:
				record["line_name"] = line_name

			if "point_number" in record:
				point_number = record["point_number"]
			elif point_number:
				record["point_number"] = point_number

			if shot_range:
				shot = int(record["point_number"])
				if not (shot >= min(shot_range) and shot <= max(shot_range)):
					continue

			if "doy" in record:
				doy = record["doy"]
			elif doy:
				record["doy"] = doy

			if "time" in record:
				time = record["time"]
			elif time:
				record["time"] = time

			line_count = line_count + 1
			if progress_callback and (line_count % callback_every) == 0:
				if progress_callback(line_count) == True:
					# If True, we should abort.
					# And we do so with whatever we have so far.
					return records

			if only_records:
				if not record["record_type"] in only_records:
					continue

			records.append(record)

		return records


def parse_fwr (string, widths, start=0):
	"""Parse a fixed-width record.

	string: the string to parse.
	widths: a list of record widths, must be strictly positive.
	start: optional start index.

	Returns a list of strings.
	"""
	results = []
	current_index = start
	for width in widths:
		results.append(string[current_index : current_index + width])
		current_index += width

	return results

def parse_p190_header (string):
	"""Parse a generic P1/90 header record.

	Returns a dictionary of fields.
	"""
	names = [ "record_type", "header_type", "header_type_modifier", "description", "data" ]
	record = parse_fwr(string, [1, 2, 2, 27, 48])
	return dict(zip(names, record))

def parse_p190_type1 (string):
	"""Parse a P1/90 Type 1 non receiver group record."""
	names = [ "record_type", "line_name", "spare1",
	"vessel_id", "source_id", "tailbuoy_id", "point_number",
	"latitude", "longitude", "easting", "northing", "water_depth",
	"doy", "time", "spare2" ]
	record = parse_fwr(string, [1, 12, 3, 1, 1, 1, 6, 10, 11, 9, 9, 6, 3, 6, 1])
	return dict(zip(names, record))

def parse_p190_rcv_group (string):
	"""Parse a P1/90 Type 1 receiver group record."""
	names = [ "record_type",
	"rcv_group_number1", "easting1", "northing1", "depth1",
	"rcv_group_number2", "easting2", "northing2", "depth2",
	"rcv_group_number3", "easting3", "northing3", "depth3",
	"streamer_id" ]
	record = parse_fwr(string, [1, 4, 9, 9, 4, 4, 9, 9, 4, 4, 9, 9, 4, 1])
	return dict(zip(names, record))

def parse_line (string):
	type = string[0]
	if string[:3] == "EOF":
		return
	if type == "H":
		return parse_p190_header(string)
	elif type == "R":
		return parse_p190_rcv_group(string)
	else:
		return parse_p190_type1(string)


def p190_type(type, records):
	return [ r for r in records if r["record_type"] == type ]

