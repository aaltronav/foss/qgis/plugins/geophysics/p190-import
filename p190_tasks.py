
import os
import qgis.core
from qgis.core import QgsTask
from . import p190, p190_layer


class P190ImportTask(QgsTask):
	def __init__(self, description, filename, only_records, shot_range, iface):
		super().__init__(description, QgsTask.CanCancel)

		self.iface = iface
		self.filename = filename
		self.only_records = only_records
		self.shot_range = shot_range
		self.records = None
		self.exception = None

	def run(self):
		"""
		Read the P1/90 file and return a list of records in self.records.
		Reports progress and obeys self.cancel()
		"""

		try:

			self.records = p190.from_file(self.filename, only_records=self.only_records, shot_range=self.shot_range, progress_callback=self.get_callback())

		except Exception as e:

			self.exception = e
			self.cancel()

		return not self.isCanceled()

	def finished(self, result):
		if result:
			if self.records:
				pass

			else:
				self.iface.messageBar().pushMessage("Task finished successfully but no records read from {}".format(self.filename))

		else:
			if self.exception is None:
				print("Task probably cancelled")
			else:
				raise self.exception

	def get_callback(self):

		line_length = 82 # P1/90 are fixed width lines
		file_size = os.path.getsize(self.filename)
		num_lines = int(file_size/line_length) if file_size > 0 else 1

		def callback(line_count):
			self.setProgress((line_count/num_lines)*100)
			return self.isCanceled()

		return callback
