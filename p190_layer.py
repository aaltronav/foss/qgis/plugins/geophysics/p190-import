#!/usr/bin/python3

from datetime import datetime
from qgis.PyQt.QtCore import QVariant
from qgis.core import QgsProject, QgsVectorLayer, QgsField
from qgis.core import QgsFeature, QgsGeometry, QgsPointXY

def create(name, records, progress=None, **args):
	layer = QgsVectorLayer("Point", name, "memory")
	provider = layer.dataProvider()

	attribute_definitions = {
		"record_type": QVariant.String,
		"line_name": QVariant.String,
		"vessel_id": QVariant.String,
		"source_id": QVariant.String,
		"tailbuoy_id": QVariant.String,
		"point_number": QVariant.Int,
		"water_depth": QVariant.Double,
		"doy": QVariant.Int,
		"time": QVariant.String,
		"rcv_group_number": QVariant.Int,
		"streamer_id": QVariant.String,
		"tstamp": QVariant.String
	}

	provider.addAttributes( QgsField(key, type) for key, type in attribute_definitions.items() )
	layer.updateFields()

	if "crs" in args:
		crs = args["crs"]
		layer.setCrs(crs)
	else:
		crs = None


	if "use_grid" in args:
		use_grid = args["use_grid"]

	if progress:
		progress.setMaximum(len(records))

	c = 0
	features = []
	for record in records:
		c += 1
		if progress:
			progress.setValue(c)

		if record["record_type"] in "H":
			continue

		if record["record_type"] == "R":
			if not use_grid or not crs:
				raise ValueError("Cannot import ‘R’ (receiver) records unless using a grid projection")

			for n in range(3):
				n+=1
				try:
					x = float(record["easting{}".format(n)])
					y = float(record["northing{}".format(n)])
				except ValueError:
					continue

				feature = QgsFeature()
				feature.setAttributes([
					record["record_type"], record["line_name"], None,
					None, None, record["point_number"], None,
					int(record["doy"]), record["time"],
					int(record["rcv_group_number{}".format(n)]),
					record["streamer_id"],
					tstamp(record)
				])
				feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x, y)))
				features.append(feature)

		else:
			if use_grid:
				x = float(record["easting"])
				y = float(record["northing"])
			else:
				y = dms(record["latitude"])
				x = dms(record["longitude"])

			try:
				point_number = int(record["point_number"])
			except ValueError:
				point_number = None

			try:
				water_depth = int(record["water_depth"])
			except ValueError:
				water_depth = None

			try:
				doy = int(record["doy"])
			except ValueError:
				doy = None

			feature = QgsFeature()
			feature.setAttributes([ attr for attr in [
				record["record_type"], record["line_name"], record["vessel_id"], record["source_id"], record["tailbuoy_id"], point_number, water_depth, doy, record["time"], None, None, tstamp(record)
			]])
			feature.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(x, y)))
			features.append(feature)

	provider.addFeatures(features)
	layer.updateExtents()

	return layer

def group(name, parent=None):
	if parent is None:
		return QgsProject.instance().layerTreeRoot().addGroup(name)
	else:
		return parent.addGroup(name)

def add(layer, parent=None):
	if parent is None:
		QgsProject.instance().addMapLayer(layer)
	else:
		QgsProject.instance().addMapLayer(layer, False)
		parent.addLayer(layer)


def dms(value):
	# 591544.61N
	hemisphere = 1 if value[-1] in "NnEe" else -1
	seconds = float(value[-6:-1])
	minutes = int(value[-8:-6])
	degrees = int(value[:-8])

	return (degrees + minutes/60 + seconds/3600) * hemisphere

def tstamp(record, year=None):
	if year is None:
		year = datetime.today().year

	in_fmt = "%Y %j %H%M%S"
	out_fmt = "%Y-%m-%d %H:%M:%S"

	try:
		ts = datetime.strptime("{} {} {}".format(year, record["doy"], record["time"]), in_fmt)
		return ts.strftime(out_fmt)
	except ValueError:
		return None
